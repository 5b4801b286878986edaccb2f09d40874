#include <stdlib.h>
#include <malloc.h>
#include <stdint.h>
#include <string.h>

void *aligned_realloc(void *ptr, size_t align, size_t size) {
  if (!ptr) return aligned_alloc(align, size);

  // according to standard c in this line    char *x = malloc(77)
  // you can't access the 78th element
  // but that's not how real memory allocators work
  size_t usable = malloc_usable_size(ptr);
  if (!((uintptr_t)ptr & (align - 1))) {
    if (usable == size) return ptr;
    if (usable > size) {
#ifdef MY_COMPILER_IS_AN_IDIOT
      // this is not needed for gcc/clang/icc/tcc/probably any compiler
      void *tmp = ptr;
      if (!(ptr = realloc(tmp, size))) return NULL;
#ifdef MY_LIBC_IS_SHIT
      // this is not needed for any libc that supports malloc_usable_size
      if ((uintptr_t) ptr & (align - 1))
#endif
#endif
      return ptr;
    }
  }

  void *newptr = aligned_alloc(align, size);
  if (!newptr) return NULL;
  memcpy(newptr, ptr, usable < size ? usable : size);
  free(ptr);

  return newptr;
}

#include <stdio.h>
int main() {
  char *ptr1 = aligned_alloc(32, 1000);
  char *ptr2 = aligned_realloc(ptr1, 32, 1001);
  ptr2[1000] = 'x';
  char *ptr3 = aligned_realloc(ptr2, 32, 2000);
  printf("%p %p %p\n", ptr1, ptr2, ptr3);
  free(ptr3);
  return 0;
}
